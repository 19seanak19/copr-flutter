cd $(rpm --eval '%{_sourcedir}')
git clone https://github.com/flutter/flutter.git
cd flutter
git checkout stable
cd ..
mv flutter flutter-$1
zip -r $1.zip ./flutter-$1
rm -rf flutter-$1
