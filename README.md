# flutter-dart

This repo contains what's required for a Flutter rpm, hosted on Copr. 

This is an unofficial packaging, so please do not bother the Flutter developers with any issues from using this package.

## Installing

```
dnf copr enable 19seanak19/flutter
dnf install flutter
```

## Uninstalling

```
dnf uninstall flutter
dnf copr disable 19seanak19/flutter
```

## Updating the package

This is more here to help me update, but this is how to update to a new Dart version.

1. Update the "Version" field in `flutter.spec`
2. Update the "VERSION" variable in `.copr/Makefile`
