Name:           flutter
Version:        3.3.10
Release:        1%{?dist}
Summary:        Flutter SDK

License:        BSD
URL:            https://flutter.dev

Source0:        https://github.com/flutter/flutter/archive/refs/tags/%{version}.zip

ExcludeArch:    s390x ppc64

BuildRequires:  curl git glibc sed unzip
Requires:       bash clang cmake coreutils curl file git glibc gtk3-devel mesa-libGLU ninja-build pkg-config unzip which xz xz-devel zip

%description
Flutter SDK libraries and command-line tools for building Flutter-based mobile, web, and desktop applications.
This is an unofficial package.

%global debug_package %{nil}
%define _build_id_links none

%prep
%autosetup

%build
echo 'Building for %{_arch}'
./bin/flutter

%install
mkdir -p %{buildroot}%{_libdir}/flutter
cp -r . %{buildroot}%{_libdir}/flutter

%check
echo -e 'void main() {\n  print(r"Hello from Dart");\n}' >> hello_test.dart
%{buildroot}%{_libdir}/flutter/bin/dart hello_test.dart
rm hello_test.dart
%{buildroot}%{_libdir}/flutter/bin/flutter --help

%post
%{__ln_s} -f %{_libdir}/flutter/bin/dart %{_bindir}/dart
%{__ln_s} -f %{_libdir}/flutter/bin/flutter %{_bindir}/flutter
groupadd -f flutter
chown -fR root:flutter %{_libdir}/flutter
usermod -a -G flutter root
usermod -a -G flutter $SUDO_USER
su - $SUDO_USER -c "git config --global --add safe.directory %{_libdir}/flutter"
chmod -R g+w %{_libdir}/flutter

%postun
case "$1" in
    0)
        rm -f %{_bindir}/dart
        rm -f %{_bindir}/flutter
        groupdel flutter
        su - $SUDO_USER -c "git config --global --unset-all safe.directory %{_libdir}/flutter"
    ;;
esac

%files
%{_libdir}/flutter
%license LICENSE

%changelog
* Fri Jan 6 2023 Sean Kimball - 3.3.10-1
- Update to Flutter 3.3.10
* Tue Nov 22 2022 Sean Kimball - 3.3.9-1
- Update to Flutter 3.3.9
* Tue Nov 22 2022 Sean Kimball - 3.3.8-1
- Initial package creation
